package labs.wuwuwu.venzor.cesar.canvas

import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.Typeface
import android.graphics.pdf.PdfDocument
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createPDFDocument()

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        var itemThatWasClicked = item?.itemId

        if (itemThatWasClicked == R.id.action_generate_pdf) {
            createPDFDocument()
            return true
        }

        if (itemThatWasClicked == R.id.action_help) {
            Toast.makeText(this, "Help", Toast.LENGTH_SHORT).show()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    fun createPDFDocument () {


        val fileName = "/sdcard/halls.pdf"
        val filePath = File(fileName)

        // create new document
        var document = PdfDocument()

        // create a page description
        var pageInfo = PdfDocument.PageInfo.Builder(842, 595, 1).create()

        // start a page
        var page = document.startPage(pageInfo)

        var canvas = page.canvas

        var paint = Paint()
        var textPaint = Paint()

        textPaint.color = Color.BLACK
        paint.color = Color.BLACK

        canvas.drawText("Business Model Canvas", 30F, 30F, paint)
        canvas.drawText("Key Partners", 50F, 60F, paint)
        canvas.drawText("Key Activities", 202F, 60F, paint)
        canvas.drawText("Key Resources", 202F, 225F, paint)
        canvas.drawText("Cost Structure", 50F, 420F, paint)
        canvas.drawText("Value Propositions", 354F, 60F, paint)
        canvas.drawText("Customer Relationships", 506F, 60F, paint)
        canvas.drawText("Channels", 506F, 225F, paint)
        canvas.drawText("Customer Segments", 658F, 60F, paint)
        canvas.drawText("Revenue Streams", 390F, 420F, paint)

        paint.style = Paint.Style.STROKE
        paint.strokeWidth = 2F

        canvas.drawRect(40F,40F,800F,550F, paint)
        canvas.drawRect(40F, 40F, 192F, 400F, paint)
        canvas.drawRect(192F, 40F, 344F, 400F, paint)
        canvas.drawRect(192F, 210F, 344F, 400F, paint)
        canvas.drawRect(344F, 40F, 496F, 400F, paint)
        canvas.drawRect(496F, 40F, 648F, 400F, paint)
        canvas.drawRect(496F, 210F, 648F, 400F, paint)
        canvas.drawRect(648F, 40F, 800F, 400F, paint)
        canvas.drawRect(380F, 400F, 800F, 550F, paint)

        paint.color = Color.YELLOW
        paint.style = Paint.Style.FILL

        canvas.drawRoundRect(RectF(48F, 70F, 160F, 100F), 5F, 5F, paint)
        canvas.drawText("Delivery", 65F, 85F, textPaint)
        canvas.drawText("services", 65F, 95F, textPaint)


        document.finishPage(page)

        try {
            document.writeTo(FileOutputStream(filePath))
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(this, "Something wrong has happened", Toast.LENGTH_SHORT).show()
        }

        document.close()
        Toast.makeText(this, "PDF file created!", Toast.LENGTH_SHORT).show()

    }
}
